import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public admob: AdMobFree, private statusBar: StatusBar){}

  	showBannerFake() {
 
        let bannerConfig: AdMobFreeBannerConfig = {
            isTesting: true, // Remove in production
            autoShow: true
            //id: Your Ad Unit ID goes here
        };
 
        this.admob.banner.config(bannerConfig);
 
        this.admob.banner.prepare().then(() => {
            // success
        }).catch(e => console.log(e));
 
    }

    launchInterstitialFake() {
 
        let interstitialConfig: AdMobFreeInterstitialConfig = {
            isTesting: true, // Remove in production
            autoShow: true
            //id: Your Ad Unit ID goes here
        };
 
        this.admob.interstitial.config(interstitialConfig);
 
        this.admob.interstitial.prepare().then(() => {
            // success
        });
 
    }

    /*showBanner() {
 		alert("Banner");
        let bannerConfig: AdMobFreeBannerConfig = {
            autoShow: true,
            id: ""
        };
 
        this.admob.banner.config(bannerConfig);
 
        this.admob.banner.prepare().then(() => {
            // success
        }).catch(e => console.log(e));
 
    }

    launchInterstitial() {
 		alert("Inter");
        let interstitialConfig: AdMobFreeInterstitialConfig = {
            autoShow: true,
            id: ""
        };
 
        this.admob.interstitial.config(interstitialConfig);
 
        this.admob.interstitial.prepare().then(() => {
            // success
        });
 
    }*/

    statusBarTrigger(){
    	this.statusBar.backgroundColorByHexString('#ff0000');	
    }
    
}
